let fatigaMaximaLocal = NaN;
let turnoActual = 0;
let unsaved = false;
let CONFIG = {
  rowsPerTurn: 2,
};

checkIfExistConfigCookie();

window.onbeforeunload = () => {
  if (unsaved) {
    return "Esta seguro que quiere recargar la web? se perderán todos los datos";
  }
};

addTurno.onclick = () => {
  if (isNaN(fatigaMaximaLocal)) {
    fatigaMaximaLocal = parseInt(fatigaMaxima.value);
    if (isNaN(fatigaMaximaLocal)) {
      alert("No has introducido la fatiga maxima");
      fatigaMaxima.focus();
      return;
    }
  }

  // if (fatigaMaximaLocal == 0) {
  //   fatigaMaximaLocal--;
  // }

  turnoActual++;

  let turno = templateTurno.cloneNode(true);
  turno.removeAttribute("id");
  turno.querySelector("[fatiga]").querySelector("[fatigaValue]").innerHTML =
    fatigaMaximaLocal-- + " (" + turnoActual + ")";

  let acciones = turno.querySelector("[acciones]");
  for (let i = 0; i < CONFIG.rowsPerTurn; i++) {
    acciones.appendChild(getAccionFromTemplate());
  }

  historial.appendChild(turno);

  window.scrollTo(0, document.body.scrollHeight);

  unsaved = true;
};

resetBtn.onclick = () => {
  if (confirm("Seguro que desea limpiar el historial?")) {
    fatigaMaximaLocal = NaN;
    historial.innerHTML = "";
    unsaved = false;
    turnoActual = 0;
  }
};

zeroBtn.onclick = () => {
  if (confirm("Seguro que desea poner a 0 la fatiga actual?")) {
    fatigaMaximaLocal = 0;
    modifiersDialog.close();
  }
};

saveBtn.onclick = () => {
  var filename = nombrePelea.value.trim();
  if (filename == "") {
    alert("Introduzca un nombre");
    nombrePelea.focus();
    return;
  }
  filename = filename.replace(" ", "_");

  createFile(filename, generateHistoryJson());

  unsaved = false;
};

openOptionsDialogBtn.onclick = () => {
  rowsPerTurn.value = CONFIG.rowsPerTurn;
  optionsDialog.show();
};

cancelOptionsBtn.onclick = () => {
  closeOptionsDialog();
};

saveOptionsBtn.onclick = () => {
  saveAndCloseOptionsDialog();
};

modifiersDialogbtn.onclick = () => {
  fatigaExtra.value = 0;
  modifiersDialog.show();
};

closeModifiersBtn.onclick = () => {
  modifiersDialog.close();
};

addFatigaExtra.onclick = () => {
  if (fatigaExtra.reportValidity()) {
    fatigaMaximaLocal += parseInt(fatigaExtra.value);
    modifiersDialog.close();
  }
};

/** TextArea autosize **/
function setAutoSizeTextArea(textArea) {
  textArea.setAttribute(
    "style",
    "height:" + (textArea.scrollHeight - 5) + "px;overflow-y:hidden;"
  );
  textArea.addEventListener("input", OnInput, false);
}

/** TextArea autosize method **/
function OnInput() {
  this.style.height = 0;
  this.style.height = this.scrollHeight - 5 + "px";
}

function addAccion(node) {
  let accion = getAccionFromTemplate();
  let acciones = getParentNodeWithAttribute(node, "turno").querySelector(
    "[acciones]"
  );
  acciones.appendChild(accion);
}

function deleteAccion(node) {
  let accion = getParentNodeWithAttribute(node, "accion");
  if (
    confirm(
      "seguro que desea borrar " + accion.querySelector("[descripcion]").value
    )
  ) {
    accion.remove();
  }
}

function getAccionFromTemplate() {
  let accion = templateAccion.cloneNode(true);
  accion.removeAttribute("id");

  setAutoSizeTextArea(accion.querySelector("[descripcion]"));

  return accion;
}

function getParentNodeWithAttribute(node, attr) {
  let accion = node.parentNode;
  if (accion.getAttribute(attr) == null) {
    return getParentNodeWithAttribute(accion, attr);
  }
  return accion;
}

function generateHistoryJson() {
  let json = [];
  var turnos = historial.querySelectorAll("[turno]");
  turnos.forEach((turno) => {
    var turnoObj = {
      fatiga: turno.querySelector("[fatiga]").querySelector("[fatigaValue]")
        .innerHTML,
      acciones: getAcciones(),
    };

    function getAcciones() {
      var list = [];
      var acciones = turno
        .querySelector("[acciones]")
        .querySelectorAll("[accion]");
      acciones.forEach((accion) => {
        list.push({
          mmr: accion.querySelector("[mmr]").value,
          accion: accion.querySelector("[descripcion]").value,
          satisfactorio: accion.querySelector("[satisfactorio]").checked,
        });
      });
      return list;
    }

    json.push(turnoObj);
  });
  return JSON.stringify(json);
}

function createFile(name, content) {
  // Create element with <a> tag
  const link = document.createElement("a");

  // Create a blog object with the file content which you want to add to the file
  const file = new Blob([content], { type: "text/plain" });

  // Add file content in the object URL
  link.href = URL.createObjectURL(file);

  const date = new Date();
  var dateStr =
    "" +
    date.getFullYear() +
    getDate2Digit(date.getMonth() + 1) +
    getDate2Digit(date.getDate());

  // Add file name
  link.download = "RQ-" + dateStr + "-" + name + ".txt";

  // Add click event to <a> tag to save file.
  link.click();
  URL.revokeObjectURL(link.href);
}

function getDate2Digit(digit) {
  if (parseInt(digit) < 10) {
    return "0" + digit;
  }
  return digit;
}

function closeOptionsDialog() {
  if (rowsPerTurn.reportValidity()) {
    optionsDialog.close();
  }
}

function saveAndCloseOptionsDialog() {
  if (rowsPerTurn.reportValidity()) {
    CONFIG.rowsPerTurn = parseInt(rowsPerTurn.value);
    optionsDialog.close();
  }
  setConfigCookie();
}

function setConfigCookie() {
  Cookies.set("options", JSON.stringify(CONFIG), { expires: 150 });
}

function checkIfExistConfigCookie() {
  const data = Cookies.get("options");
  if (data !== undefined) {
    CONFIG = JSON.parse(data);
  }
  setConfigCookie();
}
